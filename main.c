/*LIBRERIAS
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <stdbool.h>
#include <time.h>
#define ARRIBA 72
#define IZQUIERDA 75
#define DERECHA 77
#define ABAJO 80
#define ESC 27

//PROTOTIPOS DE LAS FUNCIONES
void gotoxy(int x, int y);
void ocultarCursor();
void cuadro();
void guardar_posicion();
void dibujar_cuerpo();
void borrar_cuerpo();
void teclear();
void comida1();
void comida2();
bool perder1();
bool perder2();
void esquinas();

//DECLARACION GLOBAL DE VARIABLES
int cuerpo[200][2];
int u;
int a1;
int a2;
int n = 1, tam = 4;
int dir;     //dir direccion
int x=36, y=52, i1;
int xc, yc, o;
int velocidad = 60;
int puntos=0;
char tecla;

//FUNCION PRINCIPAL MAIN
int main(){
    do{
        juego();
    }while(u==1);

 	system("cls");
	gotoxy(1,25);return 0;
}

void juego(){
        menu();
    switch(o){
        case 1:{
                Snake1();

                puntos=0;
                tam=4;
                system("cls");
        }break;
        case 2:{
                Snake2();

                puntos=0;
                tam=4;
                system("cls");
        }break;
    }
}



void menu(){
    printCuadroM();
    gotoxy(27,7);printf("BIENVENIDO A SNAKE\n\n");
    gotoxy(19,10);printf("%cEn qu%c modalidad le gustar%ca jugar?\n\n",168,130,161);
    gotoxy(29,12);printf("1. Sin bordes\n");
    gotoxy(29,13);printf("2. Con bordes\n\n");
    gotoxy(20,16);printf("Elija opci%cn con n%cmero (1 o 2): \n",162,163);
    gotoxy(53,16);scanf("%i",&o);
}


void Snake1(){
    srand(time(NULL));
    dir = (rand() % 4) + 1;
    xc = (rand() % 48) + 11;
    yc = (rand() % 23) + 6;
	system("mode con cols=73 lines=38");
 	gotoxy (30,1); printf("JUEGO SNAKE");
 	printf("\n\n");
	ocultarCursor();
 	esquinas();
 	gotoxy(xc, yc); printf("%c", 4);    //primer comida

 	while(tecla != ESC && perder1()){
 		gotoxy (3,3); printf("PUNTOS: %i",puntos);
		borrar_cuerpo();
		guardar_posicion();
        dibujar_cuerpo();
		comida1();
		teclear();
		teclear();

		Sleep(velocidad);
 	}
        system("mode con cols=73 lines=25");
        gotoxy (30,1); printf("JUEGO SNAKE");
        gotoxy(20,10); printf("OBTUVISTE UNA PUNTUACION DE: %i",puntos);
        gotoxy(24,12);printf("%cDesea regrsar al menu?: ",168);
        gotoxy(30,14);printf("SI(1)  NO(2)");
        gotoxy(19,17);printf("Elija opci%cn con n%cmero (1 o 2): \n",162,163);
        gotoxy(52,17);scanf("%i",&u);
        fflush(stdin);
}


void Snake2(){
    srand(time(NULL));
    dir = (rand()% 4) + 1;
    xc = (rand() % 71) + 39;
    yc = (rand() % 36) + 4;
	system("mode con cols=112 lines=42");
 	gotoxy (11,5); printf("JUEGO SNAKE ");
 	printf("\n\n");
	ocultarCursor();
 	printCuadroS();
 	gotoxy(xc, yc); printf("%c", 4);    //primer comida

 	while(tecla != ESC && perder2()){

 		gotoxy (3,3); printf("PUNTOS: %i",puntos);
 		gotoxy(2,7); printf("xc: %i   yc: %i",xc,yc);
 		gotoxy(2,8); printf("n: %i   yc: %i",n);
 		gotoxy(2,9); printf("x: %i   y: %i",x,y);
		borrar_cuerpo();
		guardar_posicion();
        dibujar_cuerpo();
		comida2();
		teclear();
		teclear();

		Sleep(velocidad);
 	}
        system("mode con cols=73 lines=25");
        gotoxy (30,1); printf("JUEGO SNAKE");
        gotoxy(20,10); printf("OBTUVISTE UNA PUNTUACION DE: %i",puntos);
        gotoxy(24,12);printf("%cDesea regrsar al menu?: ",168);
        gotoxy(30,14);printf("SI(1)  NO(2)");
        gotoxy(19,17);printf("Elija opci%cn con n%cmero (1 o 2): \n",162,163);
        gotoxy(52,17);scanf("%i",&u);
        fflush(stdin);
}


void printCuadroS(){
	int i,v;

	for(i=37; i < 111; i++){
		gotoxy(i, 1); printf ("%c", 205);
 		gotoxy(i, 40); printf ("%c", 205);
	}

	for(v=1; v < 40; v++){
 		gotoxy(37,v); printf ("%c", 186);
 		gotoxy(110,v); printf ("%c", 186);
	}

 	gotoxy(37,1); printf ("%c", 201);    // SI
 	gotoxy(37,40); printf ("%c", 200);   // II
 	gotoxy(110,1); printf ("%c", 187);   // SD
 	gotoxy(110,40); printf ("%c", 188);  // ID
}



void printCuadroM(){
    int i,v;
	system("mode con cols=72 lines=27");
	for(i=10; i < 61; i++){
		gotoxy(i, 5); printf ("%c", 205);
 		gotoxy(i, 21); printf ("%c", 205);
	}

	for(v=5; v < 21; v++){
 		gotoxy(10,v); printf ("%c", 186);
 		gotoxy(61,v); printf ("%c", 186);
	}

 	gotoxy(10,5); printf ("%c", 201);    // SI
 	gotoxy(10,21); printf ("%c", 200);   // II
 	gotoxy(61,5); printf ("%c", 187);   // SD
 	gotoxy(61,21); printf ("%c", 188);  // ID

}


void esquinas(){
    gotoxy(10,5); printf ("%c", 218);    // SI
 	gotoxy(10,31); printf ("%c", 192);   // II
 	gotoxy(61,5); printf ("%c", 191);   // SD
 	gotoxy(61,31); printf ("%c", 217);  // ID
}


void guardar_posicion(){
	cuerpo[n][0] = x;
 	cuerpo[n][1] = y;
 	n++;
 	if(n == tam) n = 1;
 	if(dir == 1) y--;
    if(dir == 2) y++;
    if(dir == 3) x++;
    if(dir == 4) x--;
}


void dibujar_cuerpo(){
	for(i1 = 1; i1 < tam; i1++){
 		gotoxy(cuerpo[i1][0] , cuerpo[i1][1]); printf("O");
	}
}


void borrar_cuerpo(){
	gotoxy(cuerpo[n][0] , cuerpo[n][1]); printf(" ");
}


void teclear(){
	if(kbhit()){
		tecla = getch();
	 	switch(tecla){
			case ARRIBA : if(dir != 2) dir = 1; break;
	 		case ABAJO : if(dir != 1) dir = 2; break;
	 		case DERECHA : if(dir != 4) dir = 3; break;
	 		case IZQUIERDA : if(dir != 3) dir = 4; break;
		}
 	}

}


void comida1(){
	if(x == xc && y == yc){
		xc = (rand() % 48) + 11;
 		yc = (rand() % 23) + 6;
 		tam+=2;
 		puntos += 10;
 		gotoxy(xc, yc); printf("%c", 4);
	}
}


void comida2(){
	if(x == xc && y == yc){
		xc = (rand() % 71) + 39;
 		yc = (rand() % 36) + 4;
 		if(xc <= 37) xc += 3;
 		if(xc >= 110) xc -= 3;
 		if(yc <= 1) yc += 2;
 		if(yc >= 40) yc -= 3;
 		tam+=2;
 		puntos += 10;
 		gotoxy(xc, yc); printf("%c", 4);

	}
}


bool perder2(){
	int j;
	if(y == 1 || y == 40 || x == 37 || x == 110){
		return false;
	}
 	for(j = tam - 1; j > 0; j--){
	 	if(cuerpo[j][0] == x && cuerpo[j][1] == y)
	 	return false;
	}
	return true;
}


bool perder1(){
	int j;
	if(x == 10) x=60;
    if(x == 61) x=11;
    if(y == 5) y=30;
    if(y == 31) y=6;
 	for(j = tam - 1; j > 0; j--){
	 	if(cuerpo[j][0] == x && cuerpo[j][1] == y)
	 	return false;
	}
	return true;
}


void gotoxy(int x, int y){
    COORD coord;
    coord.X=x;
    coord.Y=y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}


void ocultarCursor() {
	CONSOLE_CURSOR_INFO cci = {100, FALSE};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cci);
}
*/
//LIBRERIAS
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <stdbool.h>
#include <time.h>
#define ARRIBA 72
#define IZQUIERDA 75
#define DERECHA 77
#define ABAJO 80
#define ESC 27

//PROTOTIPOS DE LAS FUNCIONES
void gotoxy(int x, int y);
void ocultarCursor();
void printCuadroS();
void printCuadroM();
void guardar_posicion1();
void borrar_cuerpo1();
void guardar_posicion2();
void dibujar_cuerpo();
void borrar_cuerpo2();
void teclear();
void comida1();
void comida2();
bool perder1();
bool perder2();
void esquinas();
void menu();
void t();

//DECLARACION GLOBAL DE VARIABLES
int cuerpo[200][2];
int a1 [20];
int a2 [20];
int n1 = 1, n2=1, tam=4;
int dir;     //dir direccion
int x, y;
int xc, yc, o, u;
int puntos=0;
char tecla;

//FUNCION PRINCIPAL MAIN
int main(){
    do{
    o=0;
    u=0;
    menu();
    switch(o){
        case 1:Snake1();break;
        case 2:Snake2();break;
    }
        tam=4;
        puntos=0;
        n1=1;n2=1;
        o=0;
        u=0;
        system("mode con cols=73 lines=25");
        gotoxy (30,1); printf("JUEGO SNAKE");
        gotoxy(20,7); printf("OBTUVISTE UNA PUNTUACION DE: %i",puntos);
        gotoxy(24,12);printf("%cDesea regresar al menu?: ",168);
        gotoxy(30,14);printf("SI(1)  NO(2)");
        gotoxy(19,17);printf("Elija opci%cn con n%cmero (1 o 2): \n",162,163);
        gotoxy(52,17);scanf("%i",&u);
        fflush(stdin);
    }while(u==1);
 	system("cls");
	gotoxy(1,25);return 0;
}


void menu(){
    printCuadroM();
    gotoxy(27,7);printf("BIENVENIDO A SNAKE\n\n");
    gotoxy(20,10);printf("%cEn qu%c modalidad le gustar%ca jugar?\n\n",168,130,161);
    gotoxy(29,12);printf("1. Sin bordes\n");
    gotoxy(29,13);printf("2. Con bordes\n\n");
    gotoxy(20,16);printf("Elija opci%cn con n%cmero (1 o 2): \n",162,163);
    gotoxy(53,16);scanf("%i",&o);
    fflush(stdin);
}


void Snake1(){
    srand(time(NULL));
    x = 36;
    y = 22;
    dir = (rand()% 4) + 1;
    xc = (rand() % 48) + 11;
    yc = (rand() % 23) + 6;
	system("mode con cols=73 lines=38");
 	gotoxy (30,1); printf("JUEGO SNAKE");
 	printf("\n\n");
	ocultarCursor();
 	esquinas();
 	gotoxy(xc, yc); printf("%c", 4);    //primer comida

 	while(tecla != ESC && perder1()){
 		gotoxy (3,3); printf("PUNTOS: %i",puntos);
		borrar_cuerpo1();
		guardar_posicion1();
        dibujar_cuerpo();
		comida1();
		t();
        Sleep(60);
 	}
}


void Snake2(){
    srand(time(NULL));
    x = 70;
    y = 19;
    dir = (rand()% 4) + 1;
    xc = (rand() % 71) + 39;
    yc = (rand() % 36) + 4;
	system("mode con cols=112 lines=42");
 	gotoxy (11,5); printf("JUEGO SNAKE");
 	printf("\n\n");
	ocultarCursor();
    printCuadroS();
 	gotoxy(xc, yc); printf("%c", 4);    //primer comida

 	while(tecla != ESC && perder2()){
 		gotoxy (3,8); printf("PUNTOS: %i",puntos);
		borrar_cuerpo2();
		guardar_posicion2();
        dibujar_cuerpo();
		comida2();
		t();
		Sleep(60);
 	}
}


void printCuadroS(){
	int i,v;

	for(i=37; i < 111; i++){
		gotoxy(i, 1); printf ("%c", 205);
 		gotoxy(i, 40); printf ("%c", 205);
	}

	for(v=1; v < 40; v++){
 		gotoxy(37,v); printf ("%c", 186);
 		gotoxy(110,v); printf ("%c", 186);
	}

 	gotoxy(37,1); printf ("%c", 201);    // SI
 	gotoxy(37,40); printf ("%c", 200);   // II
 	gotoxy(110,1); printf ("%c", 187);   // SD
 	gotoxy(110,40); printf ("%c", 188);  // ID
}



void printCuadroM(){
    int i,v;
	system("mode con cols=72 lines=27");
	for(i=10; i < 61; i++){
		gotoxy(i, 5); printf ("%c", 205);
 		gotoxy(i, 21); printf ("%c", 205);
	}

	for(v=5; v < 21; v++){
 		gotoxy(10,v); printf ("%c", 186);
 		gotoxy(61,v); printf ("%c", 186);
	}

 	gotoxy(10,5); printf ("%c", 201);    // SI
 	gotoxy(10,21); printf ("%c", 200);   // II
 	gotoxy(61,5); printf ("%c", 187);   // SD
 	gotoxy(61,21); printf ("%c", 188);  // ID

}


void esquinas(){
    gotoxy(10,5); printf ("%c", 218);    // SI
 	gotoxy(10,31); printf ("%c", 192);   // II
 	gotoxy(61,5); printf ("%c", 191);   // SD
 	gotoxy(61,31); printf ("%c", 217);  // ID
}


void borrar_cuerpo1(){
	gotoxy(cuerpo[n1][0] , cuerpo[n1][1]); printf(" ");
}


void guardar_posicion1(){
	cuerpo[n1][0] = x;
 	cuerpo[n1][1] = y;
 	n1++;
 	if(n1 == tam) n1 = 1;
 	if(dir == 1) y--;
    if(dir == 2) y++;
    if(dir == 3) x++;
    if(dir == 4) x--;
}


void borrar_cuerpo2(){
	gotoxy(cuerpo[n2][0] , cuerpo[n2][1]); printf(" ");
}


void guardar_posicion2(){
	cuerpo[n2][0] = x;
 	cuerpo[n2][1] = y;
 	n2++;
 	if(n2 == tam) n2 = 1;
 	if(dir == 1) y--;
    if(dir == 2) y++;
    if(dir == 3) x++;
    if(dir == 4) x--;
}


void dibujar_cuerpo(){
	int i;
	for(i = 1; i < tam; i++){
 		gotoxy(cuerpo[i][0] , cuerpo[i][1]); printf("O");
	}
}

void t(){
    teclear();
    teclear();
}
void teclear(){
	if(kbhit()){
		tecla = getch();
	 	switch(tecla){
			case ARRIBA : if(dir != 2) dir = 1; break;
	 		case ABAJO : if(dir != 1) dir = 2; break;
	 		case DERECHA : if(dir != 4) dir = 3; break;
	 		case IZQUIERDA : if(dir != 3) dir = 4; break;
		}
 	}

}


void comida1(){
	if(x == xc && y == yc){
		xc = (rand() % 48) + 11;
 		yc = (rand() % 23) + 6;
 		tam+=2;
 		puntos += 10;
 		gotoxy(xc, yc); printf("%c", 4);
	}
}


void comida2(){
	if(x == xc && y == yc){
		xc = (rand() % 71) + 39;
 		yc = (rand() % 36) + 4;
 		if(xc <= 37) xc += 3;
 		if(xc >= 110) xc -= 3;
 		if(yc <= 1) yc += 2;
 		if(yc >= 40) yc -= 3;
 		tam+=2;
 		puntos += 10;
 		gotoxy(xc, yc); printf("%c", 4);
 		gotoxy(2,7); printf("xc: %i   yc: %i",xc,yc);
	}
}


bool perder2(){
	int j;
	if(y == 1 || y == 40 || x == 37 || x == 110)
		return false;
 	for(j = tam - 1; j > 0; j--){
	 	if(cuerpo[j][0] == x && cuerpo[j][1] == y)
	 	return false;
	}
	return true;
}


bool perder1(){
	int j;
	if(x == 10) x=60;
    if(x == 61) x=11;
    if(y == 5) y=30;
    if(y == 31) y=6;
 	for(j = tam - 1; j > 0; j--){
	 	if(cuerpo[j][0] == x && cuerpo[j][1] == y)
	 	return false;
	}
	return true;
}


void gotoxy(int x, int y){
    COORD coord;
    coord.X=x;
    coord.Y=y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}


void ocultarCursor() {
	CONSOLE_CURSOR_INFO cci = {100, FALSE};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cci);
}
